import React, {useState} from 'react'
import axios from "axios"

const RegisterContainer = ({render}) => {

    const [result, setResult] = useState({});
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");
   
    const handleReg = async (userData) =>{
        setResult({})
        setError("")
        setLoading(true)
        try{
            const webRes = await axios.post("https://api.victoryfor.dev/firewatch/v1/users/register", userData);
            setResult(webRes.data); 
       }catch(e){
           
            if(e.response){
                setError(e.response.data.message);
            }else{
                setError("Failed to register");
            }
       }

       setLoading(false)
        
    }

    return (
        <div>
            {render(handleReg, { result, loading, error} )}
        </div>
    )
}

export default RegisterContainer