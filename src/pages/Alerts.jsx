import React, { useEffect, useState } from 'react'
import { Tabs } from 'antd'


import AlertContainer from '../containers/AlertContainer'
import AlertList from '../components/AlertList/AlertList'



const AlertsPage = () => {
    return (
        <div>

            <h1>Alert</h1>

            <AlertContainer render={({status}) => {    
                return (<AlertList status={status} />)
            }} />
            
        </div>
    )
}

export default AlertsPage