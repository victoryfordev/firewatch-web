import React, { useEffect, useState } from 'react'
import { Tabs } from 'antd'


import LoginContainer from '../containers/LoginContainer'
import LoginForm from '../components/LoginForm'

import RegisterContainer from '../containers/RegisterContainer'
import RegisterForm from '../components/RegisterForm'


const LoginPage = () => {
    return (
        <div>

            <h1>Login</h1>
            <Tabs>
                <Tabs.TabPane tab='Login' key={1}>
                    <LoginContainer render={(handle, status) => {
                        return (<LoginForm onLogin={handle} status={status} />)
                    }} />
                </Tabs.TabPane>

                <Tabs.TabPane tab="Register" key={2}>
                    <RegisterContainer render={(handle, status) => {
                        return (<RegisterForm onRegister={handle} status={status} />)
                    }} />
                </Tabs.TabPane>


            </Tabs>
        </div>
    )
}

export default LoginPage