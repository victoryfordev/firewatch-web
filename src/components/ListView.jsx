import { Card, Progress, Row, Col } from "antd"
import { FireFilled } from '@ant-design/icons';
import FireSideBarCard from "./FireSidebarCard";

const ListView = ({fires}) => {
    
    const renderFires = () => {
       // console.log(fires);
        if(!fires.map) return (<div></div>);
        
        return fires.map((fire, index) => {
            //console.log(fire)
            return (
                
                    <Col span={12} key={index}>
                        <FireSideBarCard fire={fire} key={index}/>
                    </Col>
                
            )
        })
    };

    return (
        <div style={{
             color: 'white', textAlign: 'left', overflow: 'auto',
            position: 'fixed', zIndex: 2, height: '65%', padding: 3, borderRadius: 10, top: 300, left: 0, right: 0, backgroundColor: '#f2f2f2'
        }}>
            <Row>
             {renderFires()}
             </Row>
        </div>
    )
};

export default ListView;