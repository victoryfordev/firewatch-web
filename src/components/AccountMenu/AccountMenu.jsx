import { Divider, Menu,Button,PageHeader } from "antd"
import {FireOutlined} from "@ant-design/icons"
import {useState} from 'react'
import { useHistory } from 'react-router-dom'
import './style.css'
const AccountMenu = ({ fires, onSelect }) => {

    const [collapse, setCollapse] = useState(true)
    const itemStyle = {fontSize: '14pt'}
    const history = useHistory();
    return (
        <div >
            <Menu mode="horizontal" className="account-container" >
                <Menu.Item key="0" ><FireOutlined style={{fontSize:'20pt', marginTop: 0, padding: 0}}></FireOutlined></Menu.Item>
                <Menu.Item key="1" style={itemStyle} >Fire Map</Menu.Item>
                <Menu.Item key="2" style={itemStyle} >Fire Twitter</Menu.Item>
                <Menu.Item key="3" style={itemStyle} onClick={()=>{history.push("/alerts")}}>Alerts</Menu.Item>
                <Menu.Item key="4" style={itemStyle} onClick={()=>{history.push("/accounts")}} >Account</Menu.Item>
                <Menu.Item key="5" style={itemStyle}>About</Menu.Item>
            </Menu>
        </div>
    )
};

export default AccountMenu;