import React, { useEffect, useState} from 'react'
import { Card, Progress,Popover } from "antd"
import axios from "axios"
import useApi from '../../hooks/useAPI'

const FireContainer = ({render}) => {
    const api = useApi();
    const [fires, setFires] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");
    const getFires = async () =>{
       try{
            const webRes = await api("https://api.victoryfor.dev/firewatch/v1/fires", "get", {});
            setFires(webRes.data.data)  
       }catch(e){
            if(e.data){
                setError(e.data.data.message);
            }else{
                setError("Failed to login");
            }
       }
    }

    useEffect(()=>{
        getFires();
    }, []);

    return (
        <div>
            {render(fires)}
        </div>
    )
}

export default FireContainer