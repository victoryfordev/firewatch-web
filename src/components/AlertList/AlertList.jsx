import React, { useEffect } from 'react'
import { Card, Form, Input, Spin, message, Col, Row } from 'antd'


const AlertListItem = ({index, alert }) => {
    return (
        <Col span={10}>
            <Card title={"Alert #" + (index+1)}>
                <p>Contact Method: {alert.alert_type}</p>
                <p>Trigger: {alert.alert_trigger}</p>
                <p>Distance Range: {alert.alert_range}</p>
            </Card>
        </Col>
    )
}

const AlertList = ({ status }) => {

    const { result, loading, error } = status;

    const renderList = (alerts) => {
        if(!alerts){
            alerts = []
        }
        return alerts.map((a,i) => {
            return <AlertListItem alert={a} index={i}/>
        })
    }

    return (
        <Spin spinning={loading}>
            <Row >

                {renderList(result.data)}
            </Row>


        </Spin>
    )

}

export default AlertList