import logo from './logo.svg';
import './App.css';
import Main from "./pages/Main"
import AlertsPage from "./pages/Alerts"
import 'antd/dist/antd.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import AccountPage from './pages/Account';

function App() {
  return (
    <div className="App">
     
      <Router>
      <Main />
      </Router>
    </div>
  );
}

export default App;
