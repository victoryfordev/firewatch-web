import React, { useEffect, useState} from 'react'
import axios from "axios"
import useAuth from '../../hooks/useAuth';
import useApi from '../../hooks/useAPI';

const UserContainer = ({render}) => {

    const [result, setResult] = useState({});
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");

    const api = useApi()

    useEffect(()=>{
        getUser()
    },[])

    const getUser = async (email, password) =>{
        setResult({})
        setError("")
        setLoading(true)
        try{
            const webRes = await api("https://api.victoryfor.dev/firewatch/v1/users","get",{});
            setResult(webRes.data); 
       }catch(e){
            if(e.response){
                setError(e.response.data.message);
            }else{
                setError("Failed to get user");
            }
       }

       setLoading(false)
        
    }

    return (
        <div>
            {render({ result, loading, error} )}
        </div>
    )
}

export default UserContainer