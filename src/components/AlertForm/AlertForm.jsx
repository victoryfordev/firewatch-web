import React, { useState, useEffect } from 'react'

import { Button, Input, Form, Spin, Select} from 'antd'


const AlertForm = ({ onCreate, status,mapSelect}) => {
    const [form] = Form.useForm()

    useEffect(()=>{
        const strLoca = `${mapSelect.lat},${mapSelect.lon}`
        form.setFieldsValue({"alert_data": strLoca})
    }, [mapSelect])
    return (
        <Spin spinning={false}>
            <Form onFinish={onCreate} form={form}>
                <Form.Item
                    label="Contact Method"
                    name="alert_type"

                    rules={[
                        {
                            required: true,
                            message: 'Please select a method as contact',
                        },
                    ]}
                >
                    <Select>
                        <Select.Option value="EMAIL">Email</Select.Option>
                        <Select.Option value="SMS">SMS</Select.Option>
                    </Select>
       
                </Form.Item>


                <Form.Item
                    label="Location"
                    name="alert_data"

                    rules={[
                        {
                            required: true,
                            message: 'Please input the contact method',
                        },
                    ]}
                >
                    <Input></Input>
       
                </Form.Item>

                <Form.Item
                    label="Contact Info"
                    name="alert_dest"

                    rules={[
                        {
                            required: true,
                            message: 'Please input the contact method',
                        },
                    ]}
                >
                    <Input></Input>
       
                </Form.Item>

                <Form.Item
                    label="Trigger"
                    name="alert_trigger"

                    rules={[
                        {
                            required: true,
                            message: 'Please input the contact method',
                        },
                    ]}
                >
                    <Select>
                        <Select.Option value="ALERT">Emergency Alerts</Select.Option>
                        <Select.Option value="UPDATE">Fire Updated</Select.Option>
                        <Select.Option value="NEW">New Fire</Select.Option>
                    </Select>
       
                </Form.Item>


            </Form>
        </Spin>
    )

}

export default AlertForm