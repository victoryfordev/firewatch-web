import React, { useEffect } from 'react'
import { Card, Form, Input, Spin, message,Button } from 'antd'


const AccountInfo = ({ status }) => {

    const { result, loading, error } = status;

    const [form] = Form.useForm()

    useEffect(() => {
       
        if(result.data){
            console.log(result.data)
            form.setFieldsValue(result.data)
        }
    }, [result]);
    const handle = () =>{
        
    }

    return (
        <Spin spinning={loading}>
            <Form

                name="basic"
                form={form}
                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={handle}
            >
                 <Form.Item
                    label="Name"
                    name="user_name"

                    rules={[
                        {
                            required: false,
                            message: 'Please input your name!',
                        },
                    ]}
                ><Input />
                </Form.Item>
                <Form.Item
                    label="Email"
                    name="user_email"

                    rules={[
                        {
                            required: false,
                            message: 'Please input your email!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="user_password"

                    rules={[
                        {
                            required: false,
                            message: 'Please input your password!',
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    label="Password Confirm"
                    name="user_password_confirm"

                    rules={[
                        {
                            required: false,
                            message: 'Please confirm your password!',
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>

               

                <Form.Item
                    label="Phone (optional)"
                    name="user_phone"

                    rules={[
                        {
                            required: false,
                            message: 'Please input your phone!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>


                <Form.Item
                    wrapperCol={{
                        offset: 8,
                        span: 16,
                    }}
                >
                    <Button type="primary" htmlType="submit">
                        Save
                    </Button>
                </Form.Item>
            </Form>


        </Spin>
    )

}

export default AccountInfo