import React, { useState } from 'react'

import { Button, Form, Input, message , Spin} from 'antd'
import { useEffect } from 'react';

const LoginForm = ({ onLogin, status}) => {

    const handleSubmit = ({email, password}) => {
        onLogin(email, password);
    }

    const {result, loading, error} = status;

    useEffect(()=>{
        if(result['data']){
            message.success("Welcome");
        }
    }, [result])
    useEffect(()=>{
        if(error.length > 1){
            message.error(error)
        }
    }, [error])
    
    return (
        <Spin spinning={loading}>
        <Form
            
            name="basic"
            
            labelCol={{
                span: 8,
            }}
            wrapperCol={{
                span: 16,
            }}
            initialValues={{
                remember: true,
            }}
            onFinish={handleSubmit}
        >
            <Form.Item
                label="email"
                name="email"
                
                rules={[
                    {
                        required: true,
                        message: 'Please input your email!',
                    },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                label="Password"
                name="password"
                rules={[
                    {
                        required: true,
                        message: 'Please input your password!',
                    },
                ]}
            >
                <Input.Password />
            </Form.Item>

            <Form.Item
                wrapperCol={{
                    offset: 8,
                    span: 16,
                }}
            >
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
        </Spin>
    )
}

export default LoginForm