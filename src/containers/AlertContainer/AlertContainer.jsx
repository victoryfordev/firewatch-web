import React, { useEffect, useState} from 'react'
import axios from "axios"
import useAuth from '../../hooks/useAuth';
import useApi from '../../hooks/useAPI';

const AlertContainer = ({render}) => {

    const api = useApi();

    const [result, setResult] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");


    useEffect(()=>{
        getAlerts()
    },[])

    const getAlerts = async (email, password) =>{
        setResult({})
        setError("")
        setLoading(true)
        try{
            const webRes = await api("https://api.victoryfor.dev/firewatch/v1/alerts", "get", {})
            setResult(webRes.data); 
       }catch(e){
           
            if(e.response){
                setError(e.response.data.message);
            }else{
                setError("Failed to get alerts");
            }
       }

       setLoading(false)
        
    }

    const createAlert = () =>{

    }

    return (
        <div>
            {render({
                create: createAlert,
                status: { result, loading, error} 
            })}
        </div>
    )
}

export default AlertContainer