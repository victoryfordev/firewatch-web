FROM node:14
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install -g typescript
RUN yarn
COPY . .
RUN npm run build
RUN ls /usr/src/app/build

FROM nginx:alpine
COPY --from=0 /usr/src/app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80