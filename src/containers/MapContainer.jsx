import { useContext } from "react";
import { createContext, useState } from "react";

const MapContext = createContext({lat: 0, lon: 0});

export const MapContainer = ({children}) => {
    const mapState = useState();

    return (
        <MapContext.Provider value={mapState}>
            {children}
        </MapContext.Provider>
    );
};

export const useMap = () => useContext(MapContext);