import React, { useEffect, useState } from 'react'
import { Tabs } from 'antd'


import LoginContainer from '../containers/LoginContainer'
import LoginForm from '../components/LoginForm'

import RegisterContainer from '../containers/RegisterContainer'
import RegisterForm from '../components/RegisterForm'

import UserContainer from '../containers/UserContainer'
import AccountInfo from '../components/AccountInfo'

const AccountPage = () => {
    return (
        <div>

            <h1>Account</h1>
            <UserContainer render={(status) => {
                        console.log(status)
                        return (<AccountInfo status={status} />)
                    }} />
        </div>
    )
}

export default AccountPage