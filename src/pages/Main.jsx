import Map from "../components/Map"
import { useState, useEffect } from "react";
import Sidebar from "../components/Sidebar"
import DetailBar from "../components/DetailBar"
import MediaQuery from "react-responsive";
import {
    BrowserView,
    MobileView,
    isBrowser,
    isMobile
} from "react-device-detect"
import ListView from "../components/ListView";
import AccountMenu from "../components/AccountMenu";
import FireContainer from "../containers/FireContainer";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import AccountPage from "./Account";
import AlertsPage from "./Alerts";
import LoginPage from "./Login";
import NewAlert from "./NewAlert";
import { MapContainer } from "../containers/MapContainer";
const Main = () => {

    const [width, setWidth] = useState(window.innerWidth);
    const [mobile, setMobile] = useState(false);

    const [focusFire, setFocus] = useState(null);
    useEffect(() => {
        window.addEventListener('resize', () => {
            //setMobile(window.innerWidth <= 768);
            //console.log("Main - Mobile = " + mobile + " - Width = " + window.innerWidth);
        });
        return () => {
            //window.removeEventListener('resize', handleWindowSizeChange);
        }
    }, []);

    const handleFireClick = (fire) => {

        setFocus(fire);
    }

    return (
        <div>
            <MapContainer>
            <MediaQuery minWidth={750}>
                
                <AccountMenu />
                <FireContainer render={(fires) => {

                    return (
                        <div>
                            <Sidebar fires={fires} onSelect={f => handleFireClick(f)} />
                            <Map fires={fires} focus={focusFire}></Map>
                        </div>
                    )
                }} />
                <DetailBar render={()=>{
                    return (
                        <Switch>
                        <Route exact path="/">
    
                        </Route>

                        <Route exact path="/login">
                            <LoginPage/>
                        </Route>
    
                        <Route exact path="/accounts">
                            <AccountPage/>
                        </Route>
    
                        <Route exact path="/alerts">
                            <AlertsPage />
                        </Route>

                        <Route exact path="/newalert">
                            <NewAlert/>
                        </Route>
    
    
                    </Switch>
                    )
                }} />
               


            </MediaQuery>

            </MapContainer>
        </div>
    )
};

export default Main;