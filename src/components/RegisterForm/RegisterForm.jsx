import React, { useEffect } from 'react'
import { Form, Button, message, Spin, Input } from "antd"

const RegisterForm = ({onRegister, status}) => {
    const { result, loading, error } = status;

    useEffect(() => {
        if (result['data']) {
            message.success("Welcome");
        }
    }, [result])
    useEffect(() => {
        if (error.length > 1) {
            message.error(error)
        }
    }, [error])

    const handleRegister = (values) => {
        if (values['user_password'] !== values['user_password_confirm']) {
            message.warn("Passwords don't match!");
            return;
        }
        onRegister(values);
    }

    return (
        <Spin spinning={loading}>
            <Form

                name="basic"

                labelCol={{
                    span: 8,
                }}
                wrapperCol={{
                    span: 16,
                }}
                initialValues={{
                    remember: true,
                }}
                onFinish={handleRegister}
            >
                <Form.Item
                    label="Email"
                    name="user_email"

                    rules={[
                        {
                            required: true,
                            message: 'Please input your email!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="user_password"

                    rules={[
                        {
                            required: true,
                            message: 'Please input your password!',
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    label="Password Confirm"
                    name="user_password_confirm"

                    rules={[
                        {
                            required: true,
                            message: 'Please confirm your password!',
                        },
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item
                    label="Name"
                    name="user_name"

                    rules={[
                        {
                            required: true,
                            message: 'Please input your name!',
                        },
                    ]}
                ><Input />
                </Form.Item>

                <Form.Item
                    label="Phone (optional)"
                    name="user_phone"

                    rules={[
                        {
                            required: false,
                            message: 'Please input your phone!',
                        },
                    ]}
                >
                    <Input />
                </Form.Item>


                <Form.Item
                    wrapperCol={{
                        offset: 8,
                        span: 16,
                    }}
                >
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </Spin>
    )
};


export default RegisterForm;