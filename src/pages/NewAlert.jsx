import React from 'react'
import { useEffect } from 'react';
import AlertForm from '../components/AlertForm'
import AlertContainer from '../containers/AlertContainer'
import { useMap } from '../containers/MapContainer';


const NewAlert = () =>{

    const [selectResult, setSelectResult]  = useMap(); 

    useEffect(()=>{
        console.log(selectResult)
    }, [selectResult]);
    return (
        <div>
            <h1>New Alert</h1>
            <AlertContainer render={({create, status})=>{
                return ( <AlertForm onCreate={create} mapSelect={selectResult}></AlertForm>)
            }}/>
        </div>
    )
}
export default NewAlert