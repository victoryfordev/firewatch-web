import {useEffect, useState} from 'react'
import axios from 'axios';
import { useHistory } from 'react-router-dom'
const useApi = () => {
    const history = useHistory();
    const errorHandler = (error) =>{
        console.dir(error);
        if(error.response.status === 403){
            history.push('/login');
            localStorage.removeItem('firewatch-auth-token')
        }

    }

    const getToken = () => {
        const token = localStorage.getItem('firewatch-auth-token');
        if (token) {
            return token;
        }
        return false;
    }

    const makeRequest = async (url, method, body) => {
        const token = getToken();
        const headers = {
            authorization: `Bearer ${token}`
        }

        if(method === 'post' ||  method === 'put'){
            return await axios[method](url, body, {headers}).catch(errorHandler);
        }else{
            return await axios[method](url, {headers}).catch(errorHandler);
        }
    }
    
    return makeRequest;
  }

  export default useApi