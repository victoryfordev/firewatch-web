import React, {useEffect, useState} from 'react'
import './style.css'

const DetailBar = ({render}) => {
    return(
        <div className="detailbar-container">
            {render()}
        </div>
    )
}

export default DetailBar