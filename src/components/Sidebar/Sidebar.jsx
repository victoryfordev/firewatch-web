import { Card, Progress, Select } from "antd"
import { FireFilled } from '@ant-design/icons';
import FireSideBarCard from "../FireSidebarCard";
import './style.css'
const Sidebar = ({ fires, onSelect }) => {

    const renderFires = () => {
        // console.log(fires);
        if (!fires.map) return (<div></div>);

        return fires.map((fire, index) => {
            if(fire.fire_size[0].fire_size_contained >= 100) {
                return (
                    <div></div>
                )
            }
            return (
                <FireSideBarCard
                    fire={fire}
                    onSelect={onSelect}
                    key={index}
                />
            )
        })
    };

    return (
        <div className="sidebar-container">
            <Select className="sidebar-select" defaultValue="date">
              
                <Select.Option value="date">Sort by Date</Select.Option>
            </Select>
            {renderFires()}
        </div>
    )
};

export default Sidebar;