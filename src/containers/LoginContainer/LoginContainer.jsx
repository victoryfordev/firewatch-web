import React, { useEffect, useState} from 'react'
import { Card, Progress,Popover } from "antd"
import axios from "axios"

const LoginContainer = ({render}) => {

    const [result, setResult] = useState([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState("");
   
    const handleLogin = async (email, password) =>{
        setResult({})
        setError("")
        setLoading(true)
        try{
            const webRes = await axios.post("https://api.victoryfor.dev/firewatch/v1/users/login", {email, password});
            setResult(webRes.data); 
            localStorage.setItem("firewatch-auth-token", webRes.data.data.token);
       }catch(e){
           
            if(e.response){
                setError(e.response.data.message);
            }else{
                setError("Failed to login");
            }
       }

       setLoading(false)
        
    }

    return (
        <div>
            {render(handleLogin, { result, loading, error} )}
        </div>
    )
}

export default LoginContainer