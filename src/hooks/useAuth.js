import {useEffect, useState} from 'react'

const useAuth = () => {
    const [token, setToken] = useState(localStorage.getItem("firewatch-auth-token"))

    useEffect(()=>{
        //setToken(localStorage.getItem("firewatch-auth-token"))
    }, []);
    
  
    return token;
  }

  export default useAuth