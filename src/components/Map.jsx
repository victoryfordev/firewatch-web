import React, { useEffect } from 'react'
import { Card, Progress,Popover } from "antd"
import { GoogleMap, useJsApiLoader, Marker, OverlayView, Circle } from '@react-google-maps/api';
import { getTwoToneColor, setTwoToneColor } from '@ant-design/icons';
import { FireTwoTone } from '@ant-design/icons';
import { useMap } from '../containers/MapContainer';


const Map = ({ mobile, fires, focus }) => {

    const { isLoaded } = useJsApiLoader({
        id: 'google-map-script',
        googleMapsApiKey: "AIzaSyAnb_7GCSvU1AbRVgQLjqsH5e45FPBZnzw"
    })


    const containerStyle = {
        width: '100%',
        borderRadis: 25,
        borderWidth: 1,
        borderType: 'solid',
        margin: 0,
        height: mobile === true ? '322px' : '1000px',
    };
    const [selectResult, setSelectResult]  = useMap();
    const [map, setMap] = React.useState(null)
    const [center, setCenter] = React.useState({
        lat: 35.7783,
        lng: -119.417
    })

    useEffect(() => {
        if (focus) {

            const firePos = { lat: parseFloat(focus.fire_lat), lng: parseFloat(focus.fire_long) }

            setCenter(firePos);
        }
    }, [focus]);

    setTwoToneColor('#FF0000');
    const onLoad = React.useCallback(function callback(map) {
        const bounds = new window.google.maps.LatLngBounds();

        map.fitBounds(bounds);

        setMap(map)



    }, [])

    const onUnmount = React.useCallback(function callback(map) {
        setMap(null)
    }, [])
    const options = {
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
        draggable: false,
        editable: false,
        visible: true,
        zIndex: 1
    }
    const renderMarkers = (fireData) => {
        if (!fireData.map) return <div></div>;
        return fireData.map(function (fire, index) {
            const firePos = { lat: parseFloat(fire.fire_lat), lng: parseFloat(fire.fire_lon) }
           
            if (index <= 1) {
                // setCenter(firePos)
            }
            const latestSize = fire.fire_size.sort((a, b) => new Date(b.fire_size_date) - new Date(a.fire_size_date))[0]
            //console.log({lat: parseFloat(fire.fire_lat), lng: parseFloat(fire.fire_long)});
            return (

                <div>
                    <Circle zIndex={5} center={firePos} radius={Math.max((latestSize.fire_size_acres / 800000) * 75000, 1000)} options={options}>

                    </Circle >
                    <OverlayView
                        position={firePos}
                        mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
                    >
                        <div style={{ padding: 3, paddingBottom: 1, borderRadius: 5, color: 'white' }}>
                            <Popover content={<div style={{padding: 1}}><p>{fire.fire_acres} acres burned</p> <p>{fire.fire_contained}% contained</p></div>} title={fire.fire_name}>
                                <FireTwoTone style={{ color: 'red', fontSize: 25, marginBottom: 6, marginTop: 1 }}></FireTwoTone>
                            </Popover>,
                            



                        </div>

                    </OverlayView>

                </div>
            )
        })

    }

    const handleClick = (e) =>{
        const lat = e.latLng.lat()
        const lon = e.latLng.lng()
        setSelectResult({lat,lon})
    }

    return isLoaded ? (

        <GoogleMap
            onClick={handleClick}
            mapContainerStyle={containerStyle}
            center={center}
            zoom={9}

        >

            {renderMarkers(fires)}
        </GoogleMap>

    ) : <></>
}

export default React.memo(Map)
