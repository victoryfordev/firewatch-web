import { Card, Progress } from "antd"
import { FireFilled } from '@ant-design/icons';

const FireSideBarCard = ({ fire, onSelect}) => {

    const {fire_name, fire_size,fire_location, fire_contained} = fire;
    const latestSize = fire_size.sort((a, b) => new Date(b.fire_size_date) - new Date(a.fire_size_date))[0]
    return (<Card size="small" style={{ borderRadius: 5, margin: 5, height: 100}} hoverable onClick={()=>onSelect(fire)}>
        <Card.Meta
            avatar={<FireFilled color="#ee1100" style={{color: "#ee1100"}}/>}
            title={fire_name}
            description={<div><p>{latestSize.fire_size_acres || 0} acres burned</p></div>}
        />
        <Progress percent={parseInt(latestSize.fire_size_contained)} />
    </Card>)
};

export default FireSideBarCard;